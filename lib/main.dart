import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geosevice_test/utils.dart';
import 'package:geosevice_test/widgets.dart';
import 'package:http/http.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GeoService Test App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _latController = TextEditingController();
  final TextEditingController _lonController = TextEditingController();
  final TextEditingController _zController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final StreamController<Widget> _streamController = StreamController();

  int x = 0;
  int y = 0;
  int z = 0;

  Future<Uint8List> _sendRequest(
      {required int x, required int y, required int z}) async {
    final url = Uri.parse(
        "https://core-carparks-renderer-lots.maps.yandex.net/maps-rdr-carparks/tiles?l=carparks&x=$x&y=$y&z=$z&scale=1&lang=ru_RU");
    final response =
        await get(url).onError((error, stackTrace) => throw Exception());
    if (response.statusCode == 200) {
      response;
      return response.bodyBytes;
    } else {
      throw Exception();
    }
  }

  void _onPressedOkButton() {
    double lat = 0;
    double lon = 0;

    try {
      lat = double.parse(_latController.text.trim());
      lon = double.parse(_lonController.text.trim());
      z = double.parse(_zController.text.trim()).floor();

      List<double> globalPixelCoordinates =
          Utils.geographicCoordinatesToGlobalPixelCoordinates(
              lat: lat, lon: lon, z: z);

      x = (globalPixelCoordinates[0] / 256).floor();
      y = (globalPixelCoordinates[1] / 256).floor();

      _streamController.add(const CircularProgressIndicator());
      _sendRequest(x: x, y: y, z: z).then((value) {
        _streamController.add(Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [Text("[$x, $y] z= ${z.floor()}"), Image.memory(value)],
        ));
      }, onError: (_) {
        _streamController.add(const Text("Error!"));
      });
    } catch (_) {
      _formKey.currentState?.validate();
    }
  }

  Widget _formField() {
    return Form(
        key: _formKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Widgets.dataTextField(
              title: "Широта/Latitude",
              controller: _latController,
              validator: (value) =>
                  Utils.validator(value: value, regEx: [-90, 90])),
          Widgets.dataTextField(
              title: "Долгота/Longitude",
              controller: _lonController,
              validator: (value) =>
                  Utils.validator(value: value, regEx: [-180, 180])),
          Widgets.dataTextField(
              title: "Масштаб/Zoom",
              controller: _zController,
              validator: (value) => Utils.validator(value: value))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Test App")),
        body: Container(
            alignment: Alignment.topCenter,
            child: Column(children: [
              _formField(),
              ElevatedButton(
                  onPressed: _onPressedOkButton, child: const Text("OK")),
              Padding(
                  padding: const EdgeInsets.all(Widgets.padding),
                  child: StreamBuilder(
                      initialData: Container(),
                      stream: _streamController.stream,
                      builder: (context, snapshot) {
                        return snapshot.data ?? Container();
                      }))
            ])));
  }
}
