import 'dart:math';

class Utils {

  static String? validator({required String? value, List<double>? regEx}) {
    if (value == null || value.isEmpty) {
      return "Text is empty";
    } else {
      try {
        var v = double.parse(value);
        if (regEx != null && !(regEx[0] <= v && v <= regEx[1])) {
          throw Exception();
        }
        return null;
      } catch (_) {
        return "Invalid data";
      }
    }
  }

  static List<double> geographicCoordinatesToGlobalPixelCoordinates(
      {required double lat, required double lon, required int z}) {
    const eccentricity = 0.0818191908426;

    final beta = pi * lat / 180;
    final phi = (1 - eccentricity * sin(beta)) / (1 + eccentricity * sin(beta));
    final theta = tan(pi / 4 + beta / 2) * pow(phi, eccentricity / 2);
    final rho = pow(2, z + 8) / 2;

    final x = rho * (1 + lon / 180);
    final y = rho * (1 - log(theta) / pi);

    return [x, y];
  }
}