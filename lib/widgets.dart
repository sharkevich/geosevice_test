import 'package:flutter/material.dart';

class Widgets {

  static const double padding = 24;

  static Widget dataTextField(
      {required String title,
        required TextEditingController controller,
        required String? Function(String?)? validator}) {
    return Padding(
        padding: const EdgeInsets.all(padding / 2),
        child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(title),
              const SizedBox(width: padding),
              SizedBox(
                  width: 150,
                  child: TextFormField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5)),
                          constraints: const BoxConstraints(),
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: padding / 3)),
                      validator: validator,
                      controller: controller))
            ]));
  }
}